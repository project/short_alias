<?php

namespace Drupal\short_alias\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of the 'short_alias_default' widget.
 *
 * @FieldWidget(
 *   id = "short_alias_default",
 *   label = @Translation("Short Alias Source Widget"),
 *   field_types = {
 *     "short_alias"
 *   }
 * )
 */
class ShortAliasWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $default_url_value = '';
    $entity = $form_state->getFormObject()->getEntity();

    // If existing entity, check for existing short alias.
    if (!$entity->isNew() && \Drupal::currentUser()->hasPermission('administer redirects')) {

      // Get the internal path of the URL.
      $internal_path = $entity->toUrl()->getInternalPath();
      // Find short_alias to this entity.
          /** @var \Drupal\redirect\Entity\Redirect $short_alias */
      $short_alias = \Drupal::service('short_alias.repository')
        ->findByDestinationUri(["internal:/$internal_path", "entity:/$internal_path"]);

      // If a short alias exists, remove the leading slash for display. Otherwise display an empty input.
      $default_url_value =  $short_alias ? substr($short_alias->getSourcePathWithQuery(), 1) : "";
    }

    $description = 'Check this to generate a new short alias. Existing short aliases can be managed as a redirect.';
    $element['short_alias'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate new short alias'),
      '#default_value' => FALSE,//$entity->path->pathauto,
      '#description' => $description,
      '#weight' => -1,
    ];

    $element['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#placeholder' => $this->getSetting('placeholder_url'),
      '#default_value' => $default_url_value,
      '#maxlength' => 2048,
      '#required' => $element['#required'],
      // Add a trailing slash to make it more clear that a redirect should not
      // start with a leading slash.
      '#field_prefix' => \Drupal::request()->getSchemeAndHttpHost() . '/',
      '#attributes' => ['data-disable-refocus' => 'true'],
      '#disabled' => TRUE
    ];

    if (isset($form['advanced'])) {
      $element += [
        '#type' => 'details',
        '#title' => $this->t('Short alias'),
        '#open' => !empty($items[$delta]->alias),
        '#group' => 'advanced'
      ];
      $element['#weight'] = 30;
    }
    return $element;
  }
}
