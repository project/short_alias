<?php

namespace Drupal\short_alias;

use Drupal\redirect\RedirectRepository;

class ShortAliasRepository extends RedirectRepository {
  /**
     * Finds redirects based on the destination URI.
     *
     * @param string[] $destination_uri
     *   List of destination URIs, for example ['internal:/node/123'].
     *
     * @return \Drupal\redirect\Entity\Redirect[]
     *   Array of redirect entities.
     */
    public function findByDestinationUri(array $destination_uri) {
      $storage = $this->manager->getStorage('redirect');
      $result = $storage->getQuery()
        ->accessCheck(TRUE)
        ->condition('redirect_redirect.uri', $destination_uri, 'IN')
        ->condition('type', 'short_alias', 'IN')
        ->sort('created', 'DESC')
        ->range(0, 1) // Limit the result to the most recent record.
        ->execute();
      $entity_id = reset($result);
      return $storage->load($entity_id);
    }
}

