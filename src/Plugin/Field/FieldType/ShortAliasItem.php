<?php

namespace Drupal\short_alias\Plugin\Field\FieldType;

use Dflydev\Base32\Crockford\Crockford;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation for short_alias.
 *
 * @FieldType(
 *   id = "short_alias",
 *   label = @Translation("Short alias"),
 *   description = @Translation("Stores a short alias"),
 *   default_widget = "short_alias_default",
 *   default_formatter = "short_alias",
 *   no_ui = TRUE
 * )
 */
class ShortAliasItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['path'] = DataDefinition::create('string')
      ->setLabel(t('Path'));
    $properties['short_alias'] = DataDefinition::create('boolean')
      ->setLabel(t('Short alias'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return ($this->path === NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if ($this->path !== NULL) {
      $this->path = trim($this->path);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $redirect_storage = \Drupal::entityTypeManager()->getStorage('redirect');
    $entity = $this->getEntity();

    define("MAX_GENERATION_ATTEMPTS", 3);
    // If the checkbox is checked, we need to generate a new short alias.
    if ($this->short_alias) {
      for ($generation_attempt = 0; $generation_attempt < MAX_GENERATION_ATTEMPTS; $generation_attempt++) {
        $random_number = rand(1, pow(32, 5) - 1);
        $alias_path = Crockford::encode($random_number);
        $redirect_repository = \Drupal::service('redirect.repository');
        $existing_redirect = $redirect_repository->findBySourcePath($alias_path);
        if (count($existing_redirect) === 0) {
          break;
        }
      }

      if ($generation_attempt === MAX_GENERATION_ATTEMPTS) {
        \Drupal::messenger()->addError(t('Failed to generate a unique short alias.'));
        return;
      }

      $redirect = $redirect_storage->create([
        'type' => 'short_alias',
        'redirect_redirect' => 'internal:/' . $entity->toUrl()->getInternalPath(),
        'redirect_source' => $alias_path,
        'status_code' => 302,
        // 'language' => $language TODO: figure out how to get the current language
      ]);

      $redirect->save();
      $this->rid = $redirect->id();
    }
    // elseif (!$this->path) {
    //   // We will likely want to just load the most recent short alias and display it on the form
    //   // If someone tries to blank this field, we should set a message on form submit
    //   // saying that if they want to remove the short alias, then they should delete
    //   // it in redirect management
    // }
  }

}
